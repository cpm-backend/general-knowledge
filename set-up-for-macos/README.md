# Set Up for macOS

The order of following topics is your set up steps for MacOS. The reference of this README is [macOS Setup Guide](https://sourabhbajaj.com/mac-setup/).

## Xcode
[Xcode](https://developer.apple.com/xcode/) is an integrated development environment for macOS containing a suite of software development tools developed by Apple for developing software for MacOS, iOS. Download and install it from the App Store or from Apple's website. Or you can open a Terminal window and enter the following command:

```
xcode-select --install
```

## Homebrew
**Homebrew** calls itself *The missing package manager for macOS* and is an essential tool for any developer. Before you can run Homebrew you need to have the **Command Line Tools for Xcode** installed. To install Homebrew run the following in a terminal:
```
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
```
To make the Homebrew-installed programs available in your shell, you need to add your Homebrew installation location to your `$PATH`. This is done for you already on macOS 10.14 Mojave and newer. For older versions of macOS, you change your path by adding `/usr/local/bin` to your `PATH` environment variable. This can be done on a per-user basis by adjusting `PATH` in your `~/.bash_profile`. To do this, run:
```
echo 'PATH="/usr/local/bin:$PATH"' >> ~/.bash_profile
```
If you're using `zsh`, in addition to above command, you also need to run:
```
echo 'PATH="/usr/local/bin:$PATH"' >> ~/.zshrc
```
### Usage
- To install a package (or **Formula** in Homebrew vocabulary) simply type:
    ```md
    brew install [FORMULA]
    ```
- To update Homebrew's directory of formulae, run:
    ```
    brew update
    ```
- If that above command fails you can manually download the directory of formulas like this:
    ```
    cd /usr/local/Homebrew/
    git fetch origin
    git reset --hard origin/master
    ```
- To see if any of your formulas need to be updated:
    ```
    brew outdated
    ```
- To update a formula:
    ```
    brew upgrade [FORMULA]
    ```
- Homebrew keeps older versions of formulas installed on your system, in case you want to roll back to an older version. That is rarely necessary, so you can do some cleanup to get rid of those old versions:
    ```
    brew cleanup
    ```
- To see what you have installed (with their version numbers):
    ```
    brew list --versions
    ```
- To search for formulas you run. If you got permission issue, please refer to [How to fix homebrew permissions?](https://stackoverflow.com/a/46844441) or [Permission denied](https://github.com/Homebrew/brew/issues/426).
    ```
    brew search [FORMULA]
    ```
- To get more information about a formula you run:
    ```
    brew info [FORMULA]
    ```
- To uninstall a formula you can run:
    ```
    brew uninstall [FORMULA]
    ```
### Cask
- Homebrew-Cask extends Homebrew and allows you to install large binary files via a command-line tool. You can for example install applications like Google Chrome, Dropbox, VLC and Spectacle. No more downloading .dmg files and dragging them to your Applications folder.
- To see if an app is available on Cask you can search on the official Cask website. You can also search in your terminal:
    ```
    brew search [FORMULA]
    ```
- Here are some useful apps that are available on Cask.
    ```
    brew cask install \
        alfred \
        android-file-transfer \
        appcleaner \
        caffeine \
        cheatsheet \
        colloquy \
        docker \
        doubletwist \
        dropbox \
        google-chrome \
        google-hangouts \
        flux \
        1password \
        rectangle \
        sublime-text \
        superduper \
        transmission \
        valentina-studio \
        vlc
    ```

## iTerm2
**iTerm2** is an open source replacement for Apple's Terminal. It's highly customizable and comes with a lot of useful features. Use **Homebrew** to download and install:

```
brew cask install iterm2
```

Here are some suggested settings you can change or set, they are all **optional**.

- Set hot-key to open and close the terminal to `command + option + i`.
- Go to **profiles** -> **Default** -> **Terminal** -> Check silence bell to disable the terminal session from making any sound.
- Download [one of iTerm2 color schemes](https://github.com/mbadolato/iTerm2-Color-Schemes/tree/master/schemes) and then set these to your default profile colors.
- Change the cursor text and cursor color to yellow make it more visible.
- Change the font to 14pt Source Code Pro Lite. Source Code Pro can be downloaded using `brew tap homebrew/cask-fonts && brew cask install font-source-code-pro`.
- If you're using BASH instead of ZSH you can add `export CLICOLOR=1` line to your `~/.bash_profile` file for nice coloring of listings.

You might be familiar with shortcuts to skip a word (⌥) or go to start/end of the line (⌘). iTerm is not set up to work with these shortcuts by default. If you want to then open up iTerm2 **preferences** (⌘ + ,) -> **Profiles** -> **Keys** -> Click on **+** icon (add new Keyboard shortcut).

shortcut|action|Esc+
--|--|--
⌘←|Send Escape Sequence|OH
⌘→|Send Escape Sequence|OF
⌥←|Send Escape Sequence|b
⌥→|Send Escape Sequence|f

## ZSH
- The Z shell (also known as zsh) is a Unix shell that is built on top of bash (the default shell for macOS) with additional features. It's recommended to use zsh over bash. Apple replaces bash with zsh as the default shell since macOS Catalina.
- Install `zsh` using Homebrew.
    ```
    brew install zsh
    ```
- The configuration file for zsh is called `.zshrc` and lives in your home folder `~/.zshrc`.
- If you want to use framework, we recommend to use [Oh My Zsh](https://github.com/ohmyzsh/ohmyzsh) or [Prezto](https://github.com/sorin-ionescu/prezto). Note that **you should pick one of them, not use both**.
- We've also included an `env.sh` file where we store our aliases, exports, path changes etc. We put this in a separate file to not pollute our main configuration file too much. This file is found in the bottom of this page.
- To include `env.sh`, open `~/.zshrc` and add the following:
    ```sh
    source ~/<path to file>/env.sh
    ```
- The following code is an example for `env.sh`, the settings are all **optional**.
    ```sh
    #!/bin/zsh

    # Add commonly used folders to $PATH
    export PATH="/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin"

    # Specify default editor. Possible values: vim, nano, ed etc.
    export EDITOR=vim

    # File search functions
    function f() { find . -iname "*$1*" ${@:2} }
    function r() { grep "$1" ${@:2} -R . }

    # Create a folder and move into it in one command
    function mkcd() { mkdir -p "$@" && cd "$_"; }

    # Example aliases
    alias cppcompile='c++ -std=c++11 -stdlib=libc++'
    alias g='git'
    ```

## Git and Github

Please refer to this [article](https://sourabhbajaj.com/mac-setup/Git/) for setting up.

### Git Ignore (Global)
- Create the file `~/.gitignore` as shown below:
    ```gitignore
    # Folder view configuration files
    .DS_Store
    Desktop.ini

    # Thumbnail cache files
    ._*
    Thumbs.db

    # Files that might appear on external disks
    .Spotlight-V100
    .Trashes

    # Compiled Python files
    *.pyc

    # Compiled C++ files
    *.out

    # Application specific files
    venv
    node_modules
    .sass-cache
    ```
- To not track files that are almost always ignored in all Git repositories run following command.
    ```
    git config --global core.excludesfile ~/.gitignore
    ```
- You can also use a default gitignore using Curl.
    ```
    curl https://raw.githubusercontent.com/github/gitignore/master/Global/macOS.gitignore -o ~/.gitignore
    ```
