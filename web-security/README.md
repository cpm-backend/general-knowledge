# Web Security

## Hashing Algorithms
Hashing algorithms are functions that generate a **fixed-length** result (the hash, or hash value) from a given input. Ideally, a good cryptographic hash function should be like:

1. It should be fast to compute the hash value for any kind of data.
1. It should be impossible to regenerate a message from its hash value (brute force attack as the only option).
1. It should be infeasible to find two messages with the same hash (if so, we call it **collision**).
1. Every change to a message, even the smallest one, should change the hash value. It should be completely different. It's called the [avalanche effect](https://en.wikipedia.org/wiki/Avalanche_effect).

**Hashing** and **Encryption** are totally difference.
- Encryption is the practice of scrambling information in a way that only someone with a corresponding key can unscramble and read it. So we can use corresponding key to read the encrypted data, that's called **Decryption**. For encryption, the data can be recovered.
- Hashing is to calculate the value for checking data is same or not. This operation will make original data lose some information, so we cannot recover original data from hash value.
- **Salting** is an additional step during hashing, typically seen in association to hashed passwords, that adds an additional value to the end of the password that changes the hash value produced. This adds a layer of security to the hashing process, specifically against brute force attacks.

For the common use case, we would like to hash function (like MD5) to hash our database password. You type your password when you create account, we don't save your password directly. We apply hash function on your password, and save hash value. Notice that we cannot convert hash value back to raw data, MD5 is hash function instead of encryption. Next time when someone use your account and type a password, we can apply hash function again to test if value is same. Database don't care about the real password, if hash value is same then let it pass.

### MD5
MD5 is one of the most widely known hash algorithm, but now it is no longer considered safe to use for cryptographic purposes, you should not use MD5 anymore. 

### SHA Family
Secure Hash Algorithm (SHA) is a cryptographic hash function.
- **SHA-1** produces a 20-byte hash value. It has been compromised in 2005 as theoretical collisions were discovered, but its real "death" occurred in 2010 when many organizations started to recommend its replacement. Microsoft, Google, and Mozilla have stopped accepting SHA-1 SSL certificates in 2017 on their browsers, after [multiple successful attacks](https://it.slashdot.org/story/15/10/09/1425207/first-successful-collision-attack-on-the-sha-1-hashing-algorithm).
- **SHA-2** is a lot more complicated and is still considered safe. However, SHA-2 shares the same structure and mathematical operations as its predecessor (SHA-1), so it's likely that it will be compromised in the near future. As so, a new option for the future is SHA-3.
- **SHA-3** is using [Keccak](https://keccak.team/index.html) algorithm which won the NIST contest in 2009. SHA-3 is significantly faster than SHA-2. A key aspect of SHA-3 is that it was designed to easily replace SHA-2 in applications that currently use that variant. SHA-3 is the safest hashing algorithm for now.

## SSL/TLS

SSL and TLS are both cryptographic protocols that provide authentication and data encryption between servers, machines, and applications operating over a network. Secure Sockets Layer, or SSL was first developed in 1995 by Netscape but was never released because it was riddled with serious security flaws. SSL 2.0 wasn't a whole lot better, so just a year later SSL 3.0 was released. Again, it had serious security flaws.

Transport Layer Security, or TLS was based on SSL 3.0. Thus, SSL and TLS form a continuously updated series of protocols, and are often lumped together as SSL/TLS.

### Asymmetrical Cryptography

- The safest method of encryption is called asymmetrical cryptography, this requires two cryptographic keys to work properly, one public and one private. You can use the public key to encrypt the data, but need the private key to decrypt it.
- But using asymmetrical cryptography takes a lot of computing resources, so much so that if you used it to encrypt **all** the information in a communications session, your computer and connection would grind to a halt. TLS gets around this problem by only using asymmetrical cryptography at the very beginning of a communications session to encrypt the conversation the server and client have to agree on a **single session key** that they'll both use to encrypt their packets from that point forward. Encryption using a shared key is called symmetrical cryptography, it's much less computationally intensive than asymmetric cryptography.

### SSL/TLS Handshake
The following screenshot is from article [The SSL/TLS Handshake: an Overview](https://www.ssl.com/article/ssl-tls-handshake-overview/).

<img src="screenshots/handshake.png" alt="handshake"/>

1. The handshake begins when a client connects to a TLS-enabled server requesting a secure connection and the client presents a list of supported [cipher suites](https://en.wikipedia.org/wiki/Cipher_suite) (algorithmic toolkits of creating encrypted connections).
1. From this list, the server picks a cipher and hash function that it also supports and notifies the client of the decision.
1. The server usually then provides identification in the form of a digital certificate. The certificate contains the server name, the **trusted** certificate authority (CA) that vouches for the authenticity of the certificate, and the server's public encryption key.
1. The client receives the certificate, it confirms the validity of the certificate before proceeding.
1. Using the server's public key, the client and server establish a session key that both will use for the rest of the session to encrypt communication. There are several techniques for doing this. The client may use the public key to encrypt a random number that's then sent to the server to decrypt, and both parties then use that number to establish the session key. Alternately, the two parties may use what's called a [Diffie–Hellman key exchange](https://en.wikipedia.org/wiki/Diffie%E2%80%93Hellman_key_exchange) to establish the session key.

### SSL certificate

Certificates are issued by Certificate Authorities (CAs), who serve as the equivalent of a passport office when it comes to confirming identities. Organizations that want to offer services encrypted by TLS must purchase certificates from CAs, who in turn verify that the organizations are who they claim to be.

Notice that we used the phrase "**trusted** CA" in that last paragraph. Anyone can set themselves up as a certificate authority, so software manufacturers like Microsoft, Apple perform the due diligence needed to authenticate their customers. So the list of Firefox, chrome and other main browser trust is trusted CA.

The standard that defines SSL certificates is called `X.509`. This standard allows certificates to carry a lot of information beyond just the public key and the confirmed identity of the certificate owner. DigiCert is a CA whose knowledge base has a detailed breakdown of the standard.

### TLS 1.2
TLS 1.2 is the most current defined version of the protocol, and it has been for several years. It established a host of new cryptographic options for communication. However, like some previous versions of the protocol, it also allowed older cryptographic techniques to be used, in order to support older computers. Unfortunately, that opened it up to vulnerabilities, as those older techniques have become more vulnerable as time has passed and computing power has become cheaper.

In particular, TLS 1.2 has become increasingly vulnerable to so-called "man-in-the-middle" attacks, in which a hacker intercepts packets in mid-communication and sends them on after reading or altering them. It's also open to the POODLE, SLOTH, and DROWN attacks. Many of these problems have arisen in recent years, increasing the sense of urgency for updating the protocol.

### TLS 1.3
TLS 1.3 plugs a lot of these holes by jettisoning support for legacy encryption systems. There is backwards compatibility in the sense that connections will fall back to TLS 1.2 if one end isn't capable of using the newer encryptions systems on the 1.3 approved list. However, if, for instance, a man-in-the-middle attack tries to force a fallback to 1.2 in order to snoop on packets, that will be detected and the connection dropped.

There are still servers out there that are using versions of TLS even older than 1.2 — some are still using the original SSL protocol. If your sever is one of those, you should upgrade now, and just leap ahead and upgrade to the 1.3 spec.

## HTTPS

HTTPS(Hypertext Transfer Protocol Secure) is a secure extension of HTTP(Hypertext Transfer Protocol). Websites that install and configure an SSL/TLS certificate can use the HTTPS protocol to establish a secure connection with the server.

URLs are preceded with either HTTP (Hypertext Transfer Protocol) or HTTPS (Hypertext Transfer Protocol Secure). This is effectively what determines how any data that you send and receive is transmitted.

<img src="screenshots/http-vs-https.png" alt="http-vs-https"/>

## CORS

Under same-origin policy, a document hosted on server A can only interact with other documents that are also on server A. In short, the same-origin policy enforces that documents that interact with each other have the same origin. An origin is made up of the following 3 parts: the protocol, host, and port number. If you used a web browser to navigate from `http://www.example.com/foo-bar.html` to `https://www.en.example.com/hello.html` is not allowed because of the different protocol (HTTPS) and host (`en.example.com`).

Cross-Origin Resource Sharing (CORS) is a mechanism that uses additional HTTP headers to tell browsers to give a web application running at one origin, access to selected resources from a different origin. So in this case, navigating to `https://www.en.example.com/hello.html` from `http://www.example.com/foo-bar.html` could be allowed with CORS.

<img src="screenshots/same-origin.svg"/>
<img src="screenshots/cross-origin.svg"/>

### Simple Requests
Some requests don't trigger a CORS preflight. Those are called **simple requests**. It's one of the allowed methods: GET, HEAD or POST.

The CORS standard manages cross-origin requests by adding new HTTP headers to the standard list of headers. The following are the new HTTP headers added by the CORS standard:

- Access-Control-Allow-Origin
- Access-Control-Allow-Credentials
- Access-Control-Allow-Headers
- Origin
- ...

The **Access-Control-Allow-Origin** header allows servers to specify how their resources are shared with external domains. So if Server B (in above screenshot) resource can be accessed by any domain, then Server A should show the response `Access-Control-Allow-Origin: *` after getting `ImageB.jpg` from Server B. If Server B wished to restrict access to the resource to requests only from Server A, Server B would send `Access-Control-Allow-Origin: https://www.example.com`.

### Preflighted Request
**Preflighted** requests first send an HTTP request by the OPTIONS method to the resource on the other domain, to determine if the actual request is safe to send. If preflight request is success then send main request like following example:

<img src="screenshots/preflight.png" alt="preflight"/>

Let's look at the full exchange between client and server. The first exchange is the preflight request/response:

```
OPTIONS /doc HTTP/1.1
Host: bar.other
User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:71.0) Gecko/20100101 Firefox/71.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Language: en-us,en;q=0.5
Accept-Encoding: gzip,deflate
Connection: keep-alive
Origin: http://foo.example
Access-Control-Request-Method: POST
Access-Control-Request-Headers: X-PINGOTHER, Content-Type


HTTP/1.1 204 No Content
Date: Mon, 01 Dec 2008 01:15:39 GMT
Server: Apache/2
Access-Control-Allow-Origin: https://foo.example
Access-Control-Allow-Methods: POST, GET, OPTIONS
Access-Control-Allow-Headers: X-PINGOTHER, Content-Type
Access-Control-Max-Age: 86400
Vary: Accept-Encoding, Origin
Keep-Alive: timeout=2, max=100
Connection: Keep-Alive
```

OPTIONS is an HTTP/1.1 method that is used to determine further information from servers, and is a safe method, meaning that it can't be used to change the resource. Note that along with the OPTIONS request, two other request headers are sent:

```
Access-Control-Request-Method: POST
Access-Control-Request-Headers: X-PINGOTHER, Content-Type
```

The above method and header information notifies the server (`bar.other`) that when the actual request is sent, it will be POST with header `X-PINGOTHER` and ` Content-Type`. The server now has an opportunity to determine whether it wishes to accept a request under these circumstances.

And we can see the response that the server sends back indicating that the request method (POST) and request headers (`X-PINGOTHER`) are acceptable as follows. `Access-Control-Max-Age` gives the value in seconds for how long the response to the preflight request can be cached for without sending another preflight request.

```
Access-Control-Allow-Origin: https://foo.example
Access-Control-Allow-Methods: POST, GET, OPTIONS
Access-Control-Allow-Headers: X-PINGOTHER, Content-Type
Access-Control-Max-Age: 86400
```

Once the preflight request is complete, the real request is sent:

```
POST /doc HTTP/1.1
Host: bar.other
User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:71.0) Gecko/20100101 Firefox/71.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Language: en-us,en;q=0.5
Accept-Encoding: gzip,deflate
Connection: keep-alive
X-PINGOTHER: pingpong
Content-Type: text/xml; charset=UTF-8
Referer: https://foo.example/examples/preflightInvocation.html
Content-Length: 55
Origin: https://foo.example
Pragma: no-cache
Cache-Control: no-cache

<person><name>Arun</name></person>


HTTP/1.1 200 OK
Date: Mon, 01 Dec 2008 01:15:40 GMT
Server: Apache/2
Access-Control-Allow-Origin: https://foo.example
Vary: Accept-Encoding, Origin
Content-Encoding: gzip
Content-Length: 235
Keep-Alive: timeout=2, max=99
Connection: Keep-Alive
Content-Type: text/plain

[Some XML payload]
```

For more content about CORS, please refer to [MDN Cross-Origin Resource Sharing (CORS)](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS).

## Content Security Policy

Content Security Policy (CSP) is an added layer of security that helps to detect and mitigate certain types of attacks, including Cross Site Scripting (XSS) and data injection attacks. To enable CSP, you need to configure your web server to return the `Content-Security-Policy` HTTP header.

Alternatively, the `<meta>` element can be used to configure a policy, for example: 

```
<meta http-equiv="Content-Security-Policy" content="default-src 'self'; img-src https://*; child-src 'none';">
```

### XSS Attack

XSS attacks exploit the browser's trust of the content received from the server. Malicious scripts are executed by the victim's browser because the browser trusts the source of the content, even when it's not coming from where it seems to be coming from.

CSP makes it possible for server administrators to reduce or eliminate the vectors by which XSS can occur by specifying the domains that the browser should consider to be valid sources of executable scripts. A CSP compatible browser will then only execute scripts loaded in source files received from those allowlisted domains, ignoring all other script (including inline scripts and event-handling HTML attributes).

For more content about CSP, please refer to [MDN Content Security Policy (CSP)](https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP).

## OWASP Security Risks
[The Open Web Application Security Project (OWASP)](https://owasp.org/) is a nonprofit foundation that works to improve the security of software. For the following content, you can visit the [OWASP Top Ten](https://owasp.org/www-project-top-ten/) on OWASP.

### Top 10 Web Application Security Risks
1. [Injection](https://owasp.org/www-project-top-ten/OWASP_Top_Ten_2017/Top_10-2017_A1-Injection): injection flaws, such as SQL, NoSQL, OS, and LDAP injection, occur when untrusted data is sent to an interpreter as part of a command or query. The attacker's hostile data can trick the interpreter into executing unintended commands or accessing data without proper authorization.
1. [Broken Authentication](https://owasp.org/www-project-top-ten/OWASP_Top_Ten_2017/Top_10-2017_A2-Broken_Authentication): application functions related to authentication and session management are often implemented incorrectly, allowing attackers to compromise passwords, keys, or session tokens, or to exploit other implementation flaws to assume other users' identities temporarily or permanently.
1. [Sensitive Data Exposure](https://owasp.org/www-project-top-ten/OWASP_Top_Ten_2017/Top_10-2017_A3-Sensitive_Data_Exposure): many web applications and APIs do not properly protect sensitive data, such as financial, healthcare, and PII. Attackers may steal or modify such weakly protected data to conduct credit card fraud, identity theft, or other crimes. Sensitive data may be compromised without extra protection, such as encryption at rest or in transit, and requires special precautions when exchanged with the browser.
1. [XML External Entities (XXE)](https://owasp.org/www-project-top-ten/OWASP_Top_Ten_2017/Top_10-2017_A4-XML_External_Entities_(XXE)): many older or poorly configured XML processors evaluate external entity references within XML documents. External entities can be used to disclose internal files using the file URI handler, internal file shares, internal port scanning, remote code execution, and denial of service attacks.
1. [Broken Access Control](https://owasp.org/www-project-top-ten/OWASP_Top_Ten_2017/Top_10-2017_A5-Broken_Access_Control): restrictions on what authenticated users are allowed to do are often not properly enforced. Attackers can exploit these flaws to access unauthorized functionality and/or data, such as access other users' accounts, view sensitive files, modify other users' data, change access rights, etc.
1. [Security Misconfiguration](https://owasp.org/www-project-top-ten/OWASP_Top_Ten_2017/Top_10-2017_A6-Security_Misconfiguration): security misconfiguration is the most commonly seen issue. This is commonly a result of insecure default configurations, incomplete or ad hoc configurations, open cloud storage, misconfigured HTTP headers, and verbose error messages containing sensitive information. Not only must all operating systems, frameworks, libraries, and applications be securely configured, but they must be patched/upgraded in a timely fashion.
1. [Cross-Site Scripting XSS](https://owasp.org/www-project-top-ten/OWASP_Top_Ten_2017/Top_10-2017_A7-Cross-Site_Scripting_(XSS)): XSS flaws occur whenever an application includes untrusted data in a new web page without proper validation or escaping, or updates an existing web page with user-supplied data using a browser API that can create HTML or JavaScript. XSS allows attackers to execute scripts in the victim's browser which can hijack user sessions, deface web sites, or redirect the user to malicious sites.
1. [Insecure Deserialization](https://owasp.org/www-project-top-ten/OWASP_Top_Ten_2017/Top_10-2017_A8-Insecure_Deserialization): insecure deserialization often leads to remote code execution. Even if deserialization flaws do not result in remote code execution, they can be used to perform attacks, including replay attacks, injection attacks, and privilege escalation attacks.
1. [Using Components with Known Vulnerabilities](https://owasp.org/www-project-top-ten/OWASP_Top_Ten_2017/Top_10-2017_A9-Using_Components_with_Known_Vulnerabilities): components, such as libraries, frameworks, and other software modules, run with the same privileges as the application. If a vulnerable component is exploited, such an attack can facilitate serious data loss or server takeover. Applications and APIs using components with known vulnerabilities may undermine application defenses and enable various attacks and impacts.
1. [Insufficient Logging & Monitoring](https://owasp.org/www-project-top-ten/OWASP_Top_Ten_2017/Top_10-2017_A10-Insufficient_Logging%252526Monitoring): insufficient logging and monitoring, coupled with missing or ineffective integration with incident response, allows attackers to further attack systems, maintain persistence, pivot to more systems, and tamper, extract, or destroy data. Most breach studies show time to detect a breach is over 200 days, typically detected by external parties rather than internal processes or monitoring.