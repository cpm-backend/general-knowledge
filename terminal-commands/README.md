# Terminal Commands

This README is a summary for the following articles:
- [Switching From Windows to Nix or a Newbie to Linux – 20 Useful Commands for Linux Newbies](https://www.tecmint.com/useful-linux-commands-for-newbies/)
- [20 Advanced Commands for Middle Level Linux Users](https://www.tecmint.com/20-advanced-commands-for-middle-level-linux-users/)
- [20 Advanced Commands for Linux Experts](https://www.tecmint.com/20-advanced-commands-for-linux-experts/)
- [Awk Command in Linux with Examples](https://linuxize.com/post/awk-command/)

## Basic Level

### ls
The command `ls` stands for **list directory contents**, List the contents of the folder, be it file or folder, from which it runs.
- `ls -l` (or `ll`) list the content of folder, in long listing fashion.
- `ls -a`, list the content of folder, including hidden files starting with `.` like `.gitignore`.

### history
The `history` command stands for **history (event) record**, it prints the history of long list of executed commands in terminal.
```
history
```
### sudo
The `sudo` (**super user do**) command allows a permitted user to execute a command as the superuser or another user, as specified by the security policy in the sudoers list.

### mkdir
The `mkdir` (**make directory**) command create a new directory with name path. However is the directory already exists, it will return an error message "cannot create folder, folder already exists".
```
mkdir [FOLDER_NAME]
```

### touch
The `touch` command stands for **update the access and modification times of each file to the current time**. touch command creates the file, only if it doesn't exist. If the file already exists it will update the timestamp and not the contents of the file.
```
touch [FILE_NAME]
```

### chmod
The Linux `chmod` command stands for **change file mode bits**. chmod changes the file mode (permission) of each given file, folder, script, etc... according to mode asked for.

- There exist 3 types of permission on a file (folder or anything but to keep things simple we will be using file).
    - Read (r)=4
    - Write(w)=2
    - Execute(x)=1
- So if you want to give only read permission on a file it will be assigned a value of **4**, for write permission only, a value of **2** and for execute permission only, a value of **1** is to be given. For read and write permission **4 + 2 = 6** is to be given, ans so on.  
- Now permission need to be set for 3 kinds of user and usergroup. The first is **owner**, then **usergroup** and **others**. For the following example, you can use `chmod 751 abc.sh` to set it up. 
    ```
    rwxr-x--x   abc.sh
    ```
- For the following example, read, write and execute to owner and only execute to group and others.
    ```
    chmod 711 abc.sh
    ```

### chown
The Linux `chown` command stands for **change file owner and group**. Every file belongs to a group of user and a owner.
- For the following example (use `ls -l` to show up), the directory `Binary` is owned by user `server` and it belongs to usergroup `root` where as directory `terminal-commands` is owned by user `chuck` and belongs to user group `staff`.
    ```
    -rw-r--r--  1 server  root  149 Jul 19 20:45 Binary
    drwxr-xr-x  4 chuck  staff  128 Jul 23 22:31 terminal-commands
    ```
- This `chown` command is used to change the file ownership and thus is useful in managing and providing file to authorised user and usergroup only.
    ```
    > chown server:server Binary
    > ls -l
    total 2
    -rw-r--r--  1 server  root  149 Jul 19 20:45 Binary
    drwxr-xr-x  4 chuck  staff  128 Jul 23 22:31
    ```

### apt
The **Debian** based `apt` command stands for **Advanced Package Tool**. Apt is an advanced package manager for Debian based system (Ubuntu, Kubuntu, etc.), that automatically and intelligently search, install, update and resolves dependency of packages on Gnu/Linux system from command line.
- Update all packages.
    ```
    apt-get update
    ```
- Install a `mplayer` package.
    ```
    apt-get install mplayer
    ```
- For more details, please check this [article](https://www.tecmint.com/useful-basic-commands-of-apt-get-and-apt-cache-for-package-management/).

### tar
The `tar` command is a **Tape Archive** which is useful in creation of archive, in a number of file format and their extraction.
- Create tar archive file `tecmint.tar` for a directory `/home/tecmint` in current working directory. `-c` to create a new `.tar` archive file. `-v` to verbosely show the `.tar` file progress. `-f` defines file name type of the archive file.
    ```
    tar -cvf tecmint.tar /home/tecmint/
    ```
- To create a compressed `gzip` archive file we use the option as `z` (it's used for `.tar.gz` and `.tgz`). 
    ```
    tar -cvzf MyImages.tar.gz /home/MyImages
    tar cvzf MyImages.tgz /home/MyImages
    ```
- To untar or extract a tar file, just issue following command using option `x` (extract). You can also use same command to uncompress `tar.gz` archive file. If you want to untar in a different directory then use option as `-C` (specified directory).
    ```
    tar -xvf public_html.tar
    tar -xvf public_html.tar -C /home/public_html/videos/
    tar -xvf thumbnails.tar.gz
    tar -xvf thumbnails.tar.gz -C /home/public_html/videos/
    ```
- To list the contents of tar archive file, just run the following command with option `t` (list content).
    ```
    tar -tvf uploadprogress.tar
    ```
- For more content, check [here](https://www.tecmint.com/18-tar-command-examples-in-linux/).

### cat
The `cat` stands for **concatenation**. Concatenate (join) two or more plain file and/or print contents of a file on standard output.
```
cat a.txt b.txt c.txt d.txt >> abcd.txt
cat abcd.txt
```
- `>>` and `>` are called **append symbol**. They are used to append the output to a file and not on standard output. `>` symbol will delete a file already existed and create a new file hence for security reason it is advised to use `>>` that will write the output without overwriting or deleting the file.

### cp
The `cp` stands for **copy**, it copies a file from one location to another location.
```
cp /home/user/Downloads abc.tar.gz /home/user/Desktop
```
If you want to copy folder with files, run following command. `-a` preserve the specified attributes such as directory an file mode, ownership, timestamps, if possible additional attributes like context, links, xattr, all. `-v` means verbose output. `-r` to copy directories recursively.
```
cp -avr /home/vivek/letters /usb/backup
```
**Wildcards** are a shell feature that are special characters that allow you to select file names that match certain patterns of characters. `cp` can be used with wildcard characters.

Wildcard|Matches
--|--
`*`|zero or more characters
`?`|exactly one character
`[abcde]`|exactly one character listed
`[a-e]`|exactly one character in the given range
`[!abcde]`|any character that is not listed
`[!a-e]`|any character that is not in the given range
`{debian,linux}`|exactly one entire word in the options given

### mv
The `mv` command **moves** a file from one location to another location.
```
mv /home/user/Downloads abc.tar.gz /home/user/Desktop
```

### pwd
The command `pwd` stands for **print working directory**, prints the current working directory with full path name from terminal.
```
> pwd
/home/user/Downloads
```

### cd 
The command `cd` stands for **change directory**, it change the working directory to execute, copy, move write, read, etc. from terminal itself.
```
cd /home/user/Desktop
```

### rm
The command `rm` stands for remove. `rm` is used to remove files and directories.
```
rm test.txt
```
If you want to remove a directory with files, you need to use `-rf` which means recursive and forced. Notice that once you `rm -rf` a directory all the files and the directory itself is lost forever. Use it with caution.
```
rm -rf src
```

## Middle Level

### find
Search for files in the given directory, hierarchically starting at the parent directory and moving to sub-directories.
- Find all the files whose type is Markdown in a current working directory.
    ```
    find . -name *.md
    ```
- Find all the files under `/home` directory with name `test.txt`.
    ```
    find /home -name test.txt
    ```
- Find all the files whose name is `readme.md` and contains both capital and small letters (ignoring case) in a current working directory.
    ```
    find . -iname readme.md
    ```
- Find all directories whose name is `terminal-commands` in a current working directory.
    ```
    find . -type d -name terminal-commands
    ```

### grep
The `grep` command searches the given file for lines containing a match to the given strings or words.
- Search `/etc/passwd` for string `postgres`.
    ```
    grep postgres /etc/passwd
    ```
- Ignore word case and all other combination with `-i` option.
    ```
    grep -i POSTGRES /etc/passwd
    ```
- Search recursively by using `-r`.The following example is to read all files under each directory for a string `127.0.0.1`.
    ```
    grep -r "127.0.0.1" /etc/
    ```
### man
The `man` is the system's manual pager. Man provides online documentation for all the possible options with a command and its usages. Almost all the command comes with their corresponding manual pages. For example,
```
man man
```

### ps
ps (Process) gives the status of running processes with a unique Id called PID.
```
> ps
  PID TTY           TIME CMD
17619 ttys001    0:00.59 -zsh
16853 ttys002    0:00.13 /bin/zsh --login -i
```
To list status of all the processes along with process id and PID, use option `-A`(or `-e`).
```
> ps -A
PID TTY           TIME CMD
  1 ??         8:48.14 /sbin/launchd
113 ??         0:10.76 /usr/sbin/syslogd
114 ??         0:51.59 /usr/libexec/UserEventAgent (System)
117 ??         0:03.29 /System/Library/PrivateFrameworks/Uninstall.framework/Resources/uninstalld
118 ??         0:20.36 /usr/libexec/kextd
```
This command is very useful when you want to know which processes are running or may need PID sometimes, for process to be killed. You can use it with `grep` command to find customised output.
```
> ps -A | grep -i ssh
 7976 ??         0:00.02 /usr/bin/ssh-agent -l
 8022 ??         0:00.19 ssh-agent -s
18224 ??         0:00.03 ssh-agent -s
```
`-f` Displays the uid, pid, parent pid, recent CPU usage, process start time, controlling tty, elapsed CPU usage, and the associated command.
```
ps -ef | grep java
```

### kill
You can use `kill` command to kill the process.
1. You need to find out PID of the process you want to kill.
    ```
    ps -e | grep -i apache2
    1285 ttys008        00:00:00 apache2
    ```
1. Note its PID to kill. If you want to forced kill, use `kill -9`.
    ```
    kill 1285
    ```

### whereis
The `whereis` command is used to locate the Binary, Sources and Manual Pages of the command. For example, to locate the Binary, Sources and Manual Pages of the command `ls` and `kill`.
```
> whereis ls
/bin/ls
> whereis kill
/bin/kill
```

### service
The `service` command controls the starting, stopping or restarting of a **service**. This command is not default in macOS.
- Startting an Apache2 server on Ubuntu.
    ```
    service apache2 start
    ```
- Restarting a Apache2 server on Ubuntu.
    ```
    service apache2 restart
    ```
- Stopping a Apache2 server on Ubuntu.
    ```
    service apache2 stop
    ```

### alias
`alias` is a built in shell command that lets you assign name for a long command or frequently used command.
- List all aliases.
    ```
    alias
    ```
- For example, you can create alias for `ls -la` to `ll`.
    ```
    alias ll='ls -la'
    ```
- To remove alias `ll`, use the following `unalias` command.
    ```
    unalias ll
    ```

### df
Report disk usages of file system. 
```
df
```

### du
Estimate file space usage. Output the summary of disk usages by ever file hierarchically, i.e., in recursive manner. `df` only reports usage statistics on file systems, while `du`, on the other hand, measures directory contents. 
```
du
```

### echo
`echo` as the name suggest echoes a text on the standard output.
```
> echo "Hello world" 
Hello world
```
The following example is to creating a small interactive script.

1. Create a file named [hello.sh](hello.sh).
    ```sh
    #!/bin/bash 
    echo "Please enter your name:" 
    read name 
    echo "Welcome to Linux, $name!"
    ```
1. Set execute permission.
    ```
    chmod 777 hello.sh
    ```
1. Run the script.
    ```
    ./hello.sh
    ```

### passwd
This is an important command that is useful for changing own password in terminal. Obviously you need to know your current passowrd for Security reason.
```
passwd
```

### cmp
Compare two files of any type and writes the results to the standard output. By default, `cmp` Returns 0 if the files are the same. If they differ, the byte and line number at which the **first difference** occurred is reported. The following example, we compare [file1.txt](file1.txt) and [file2.txt](file1.txt).
```
cmp file1.txt file2.txt
```

### wget
`wget` is a free utility for non-interactive (can work in background) download of files from the Web. It supports HTTP, HTTPS, FTP protocols and HTTP proxies.
```
wget https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-4.17.2.tar.xz
```

### mount
`mount` command is used to mount the filesystem found on a device to big tree structure(Linux filesystem) rooted at `/`. Conversely, another command `umount` can be used to detach these devices from the Tree. You need root permission to mount a device. Now mount filesystem `sda4` to directory `/media/usb`.
```
sudo mount /dev/sda4 /media/usb
```

### head
The `head` command reads the first 10 lines of a any given file name.
```
head /etc/passwd
```
If you want to retrieve more number of lines than the default 10, then `-n` option is used along with an integer telling the number of lines to be retrieved. For example, you can read first 5 lines with `-n5`. You can also use `-5` as well.
```
head -5 /etc/passwd
```

### tail
The `tail` command allows you to display last 10 lines of any text file. You can also print the last few lines using the `-n` option as shown below.
```
tail error.log
tail -n5 error.log
tail -5 error.log
```
You can use `-f` to read the file dynamically. This is really useful for debugging.
```
tail -f error.log
```

### less
`less` command is linux utility which can be used to read contents of text file one page(one screen) per time. It has faster access because if file is large, it don't access complete file, but access it page by page.
```
less /etc/passwd
```

## Advanced Level

### ping
`ping` is a command that allows you to test the IP level connectivity of a given host on the network.
```
ping google.com
```

### lsof
`lsof` stands for **list open file**” and displays all the files that your system has currently opened. As we all know Linux/Unix considers everything as a **files** (pipes, sockets, directories, devices etc). One of the reason to use `lsof` command is when a disk cannot be unmounted as it says the files are being used. With the help of this command we can easily identify the files which are in use.
- List all open files.
    ```
    lsof
    ```
- Find processes running on specific port.
    ```
    lsof -i TCP:22
    ```
- List all the running process of open files of TCP port ranges from 1 to 1024.
    ```
    lsof -i TCP:1-1024
    ```
- Find out who's looking what files and commands.
    ```
    lsof -i -u chuck
    ```
- Kill all activity of a particular user.
    ```
    kill -9 `lsof -t -u chuck`
    ```

### curl
`curl` is a command line tool to transfer data to or from a server.
- Display the content of the URL on the terminal (GET request).
    ```
    curl https://www.google.com/
    ```
- Download a file by using `-O` or `-o`. The former will save the file in the current working directory with the same name as in the remote location, whereas the latter allows you to specify a different filename and/or location.
    ```sh
    curl -O http://fake.com/yourfile.tar # Save as yourfile.tar
    curl -o newfile.tar http://fake.com/yourfile.tar # Save as newfile.tar
    ```
- Download URLs from a file [listurls.txt](listurls.txt).
    ```
    xargs -n 1 curl -O < listurls.txt
    ```
- Send a POST request (`-X`) with header (`-H`) and data (`-d`).
    ```
    curl -X POST https://example.com/login -H 'Content-Type: application/json' -d '{"user":"admin","pass":"password"}'
    ```

### ssh
SSH (Secure Shell) is a network protocol that enables secure remote connections between two systems. 
- To connect to a remote machine, you need its IP address or name.
    ```
    ssh 210.130.57.3
    ssh test.fake.com
    ```
- Specify a username for SSH connection.
    ```
    ssh fakeuser@210.130.57.3
    ```
- By default, the SSH server listens for a connection on port **22**. You can use different port by `-p`.
    ```
    ssh test.fake.com -p 3000
    ```
- Create a SSH key pair. By default, the key pair is stored in `~/.ssh/` folder.
    ```
    ssh-keygen -t rsa
    ```
- SSH supports several public key algorithms for authentication keys.
    - `rsa`: an old algorithm based on the difficulty of factoring large numbers. A key size of at least 2048 bits is recommended for RSA; 4096 bits is better. RSA is getting old and significant advances are being made in factoring. Choosing a different algorithm may be advisable. It is quite possible the RSA algorithm will become practically breakable in the foreseeable future. All SSH clients support this algorithm.
        ```
        ssh-keygen -t rsa -b 4096
        ```

    - `dsa`: an old US government Digital Signature Algorithm. It is based on the difficulty of computing discrete logarithms. A key size of 1024 would normally be used with it. DSA in its original form is no longer recommended.
        ```
        ssh-keygen -t dsa
        ```
    - `ecdsa`: a new Digital Signature Algorithm standarized by the US government, using elliptic curves. This is probably a good algorithm for current applications. Only three key sizes are supported: 256, 384, and 521 (sic!) bits. We would recommend always using it with 521 bits, since the keys are still small and probably more secure than the smaller keys (even though they should be safe as well). Most SSH clients now support this algorithm.
        ```
        ssh-keygen -t ecdsa -b 521
        ```
    - `ed25519`: this is a new algorithm added in OpenSSH. Support for it in clients is not yet universal. Thus its use in general purpose applications may not yet be advisable.
        ```
        ssh-keygen -t ed25519
        ```
- Specifying the file name by `-f`.
    ```
    ssh-keygen -f ~/mykey-ed25519 -t ed25519
    ```
- Copying the public key to the server.
    ```
    ssh-copy-id -i ~/.ssh/mykey-ed25519 fakeuser@210.130.57.3
    ```

### awk
`awk` is a general-purpose scripting language designed for advanced text processing. It is mostly used as a reporting and analysis tool.
- Print string by using `awk`. If you click enter or type anything, it returns the same welcome string we provide. To terminate the program, press `Ctrl + D`.
    ```
    awk '{print "Hello world!"}'
    ```
- Now we have a [file](employers.txt) which includes some employers.
    ```
    Sam 20 Male 2020-07-26 15:24
    Mandy 31 Female 2020-05-04 17:56
    Andrew 27 Male 2020-07-25 17:35
    Amy 38 Female 2020-07-25 18:31
    Surya 25 Female 2020-07-24 17:10
    John 46 Male 2020-07-25 18:36
    ```
- We can get `n`-th field by using `$n`. 
    ```
    awk '{print $4}' employers.txt
    ```
- Get multiple fields. `$NF` stands for number of fields, and represents the last field.
    ```
    awk '{print $1,$3,$NF}' employers.txt
    ```
- Display the first field of each record that contains `Male` you would run the following command:
    ```
    awk '/Male/ { print $1 }' employers.txt
    ```
- To match a regex against a field, specify the field and use the **contain** comparison operator (`~`) against the pattern. We won't get any record by following command, because there's no `Male` string in 2-th field.

    ```
    awk '$2 ~ /Male/ { print $1 }' employers.txt
    ```
- To match fields that do not contain a given pattern use the `!~` operator.
    ```
    awk '$3 !~ /Male/ { print $1 }' employers.txt
    ```
- You can compare strings or numbers for relationships such as, greater than, less than, equal, and so on.
    ```
    awk '$2 > 40 { print $1 }' employers.txt
    ```
- Print out all records starting with a record that matches the first pattern until a record that matches the second pattern are matched (it seems that other patterns like `<`, `>=` are not allowed).
    ```
    awk '/Sam/,/Andrew/ { print $1 }' employers.txt
    awk '$2 == 31, $2 == 38 { print $1 }' employers.txt
    ```
- Combine two or more patterns using the logical **AND** operator (`&&`) and logical **OR** operator (`||`). `$0` returns all fields.
    ```
    awk '$2 >= 25 && $3 ~ /Male/ { print $0 }' employers.txt
    ```
- You can extract the output by using `awk`.
    ```
    date | awk '{print $2,$3,$6}'
    ```
- Awk has a number of built-in variables that contain useful information and allows you to control how the program is processed. Below are some of the most common built-in variables:
    - `NF`: the number of fields in the record.
    - `NR`: the number of the current record.
    - `FILENAME` the name of the input file that is currently processed.
    - `FS`: field separator.
    - `RS`: record separator.
    - `OFS`: output field separator.
    - `ORS`: output record separator.
- Try following command to test build-in variables.
    ```
    awk '{ print "AWK is reading", NR, "lines in", FILENAME }' employers.txt
    ```
- The default value of the field separator is any number of space or tab characters. It can be changed by setting in the `FS` variable by using option `-F`.
    ```
    awk -F "." '{ print $1 }' employers.txt
    ```
- If the program is large and complex, it is best to put it in a file and use the `-f` option to pass the file to the awk command. Notice that to define a variable for the entire program, it should be set in a `BEGIN` pattern (for the above example, you can also use `BEGIN` in one-line command).
    ```
    awk -f awk/fs.awk employers.txt
    ```
- You can use `printf` to assign print format.
    ```
    awk '{ printf "%3d. %s\n", NR, $0 }' employers.txt
    ```
- Run the following example, please refer to [square,awk](awk/square.awk).
    ```
    awk -f awk/square.awk
    ```
- You can also run an awk program as an executable by using the shebang directive and setting the awk interpreter. Please refer to [omit.awk](awk/omit.awk). 
    1. First you need to make it executable.
        ```
        chmod -x awk/omit.awk
        ```
    1. Run the script.
        ```
        ./awk/omit.awk /etc/passwd
        ```
